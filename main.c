#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "errhandle.h"


#define CHILD_THREADS_COUNT     4

typedef struct {
    unsigned size;
    char **strings;
} StringsArray_t;


void *printLines(void *v_args) {
    if (NULL == v_args) {
        errorfln("%s: unexpected argument value: null", __FUNCTION__);
        return NULL;
    }

    StringsArray_t *array = (StringsArray_t *) v_args;

    for (int i = 0; i < array->size; i += 1) {
        printf("%s\n", array->strings[i]);
    }

    return NULL;
}

int main() {
    StringsArray_t arrays[] = {
            (StringsArray_t) {
                    .size = 4,
                    .strings = (char *[]) {
                            "[1] In the town where I was born",
                            "[1] Lived a man who sailed to sea",
                            "[1] And he told us of his life",
                            "[1] In the land of submarines"
                    }
            },
            (StringsArray_t) {
                    .size = 4,
                    .strings = (char *[]) {
                            "[2] So we sailed up to the sun",
                            "[2] 'Til we found a sea of green",
                            "[2] And we lived beneath the waves",
                            "[2] In our yellow submarine"
                    }
            },
            (StringsArray_t) {
                    .size = 4,
                    .strings = (char *[]) {
                            "[3] We all live in a yellow submarine",
                            "[3] Yellow submarine, yellow submarine",
                            "[3] We all live in a yellow submarine",
                            "[3] Yellow submarine, yellow submarine"
                    }
            },
            (StringsArray_t) {
                    .size = 3,
                    .strings = (char *[]) {
                            "[4] And our friends are all aboard",
                            "[4] Many more of them live next door",
                            "[4] And the band begins to play"
                    }
            }
    };

    pthread_t threads[CHILD_THREADS_COUNT];
    unsigned started_threads_count = 0;

    int errcode = NO_ERROR;

    for (int i = 0; i < CHILD_THREADS_COUNT; i += 1) {
        errcode = pthread_create(&threads[i], NULL, printLines, &arrays[i]);
        if (NO_ERROR != errcode) {
            errorfln("failed to spawn a thread: %s", strerror(errcode));
            break;
        }
        started_threads_count += 1;
    }

    for (int i = 0; i < started_threads_count; i += 1) {
        errcode = pthread_join(threads[i], NULL);
        if (NO_ERROR != errcode) {
            errorfln("failed to join child thread: %s", strerror(errcode));
        }
    }

    return errcode;
}
